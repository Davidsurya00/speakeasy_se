import React from 'react';
import './Reminder.css';

function Reminder() {
  return (
    <div className="reminder">
      <h2>Reminder</h2>
      <ul>
        <li>Course A - Due in 1 hour</li>
        <li>Course B - Test starts in 2 hours</li>
        <li>Course B - Reading task</li>
      </ul>
    </div>
  );
}

export default Reminder;
