import React from 'react';
import Sidebar from './Sidebar';
import Reminder from './Reminder';
import './SettingsPage.css';

function SettingsPage() {
  return (
    <div className="settings-container">
      <Sidebar />
      <div className="main-content">
        <header className="header">
          <h1>SETTINGS</h1>
        </header>
        <div className="settings-content">
          <h2 className="section-title">Lesson Experience</h2>
          <div className="setting-option">
            <span>Listening Exercise</span>
            <div className="toggle-switch">
              <input type="checkbox" id="listeningExercise" className="toggle-input" />
              <label htmlFor="listeningExercise" className="toggle-label"></label>
            </div>
          </div>
          <div className="setting-option">
            <span>Motivational Messages</span>
            <div className="toggle-switch">
              <input type="checkbox" id="motivationalMessages" className="toggle-input" />
              <label htmlFor="motivationalMessages" className="toggle-label"></label>
            </div>
          </div>

          <h2 className="section-title">Appearance</h2>
          <div className="setting-option">
            <span>Dark Mode</span>
            <div className="toggle-switch">
              <input type="checkbox" id="darkMode" className="toggle-input" />
              <label htmlFor="darkMode" className="toggle-label"></label>
            </div>
          </div>

          <h2 className="section-title">Chinese</h2>
          <div className="setting-option">
            <span>Show Pinyin pronunciation</span>
            <div className="toggle-switch">
              <input type="checkbox" id="pinyinPronunciation" className="toggle-input" />
              <label htmlFor="pinyinPronunciation" className="toggle-label"></label>
            </div>
          </div>

          <h2 className="section-title">Japanese</h2>
          <div className="setting-option">
            <span>Show pronunciation</span>
            <div className="toggle-switch">
              <input type="checkbox" id="japanesePronunciation" className="toggle-input" />
              <label htmlFor="japanesePronunciation" className="toggle-label"></label>
            </div>
          </div>

          <button className="done-button">DONE</button>
        </div>
      </div>
      <Reminder />
    </div>
  );
}

export default SettingsPage;
