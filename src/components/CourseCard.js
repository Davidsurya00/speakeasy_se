import React, { useState, useEffect } from 'react';
import { getDownloadURL, ref } from 'firebase/storage';
import { storage } from './firebase'; // Pastikan impor dari file firebase.js
import './CourseCard.css';

function CourseCard({ course }) {
  const [imageUrl, setImageUrl] = useState('');

  useEffect(() => {
    if (course && course.country) {
      const fetchImage = async () => {
        try {
          const imageRef = ref(storage, `flag/${course.country}.jpg`);
          const url = await getDownloadURL(imageRef);
          console.log("Image URL fetched successfully: ", url); // Logging success
          setImageUrl(url);
        } catch (error) {
          console.error("Error fetching image URL: ", error); // Logging error
        }
      };

      fetchImage();
    }
  }, [course]);

  if (!course) {
    return <div>No course data available</div>;
  }

  return (
    <div className="course-card">
      {imageUrl ? (
        <img src={imageUrl} alt={`${course.country} Course`} />
      ) : (
        <p>Loading image...</p>
      )}
      <div className="course-info">
        <h3>{course.title}</h3>
        <p>Lesson: {course.lesson}</p>
        <p>Students: {course.students}</p>
        <p>Level: {course.level}</p>
        <p>Points: {course.points}</p>
        <p>Days: {course.days}</p>
        <button>Let's go</button>
      </div>
    </div>
  );
}

export default CourseCard;
