import React, { useState, useEffect } from 'react';
import Sidebar from './Sidebar';
import CourseCard from './CourseCard';
import ProgressCard from './ProgressCard';
import Reminder from './Reminder';
import { auth, db } from './firebase'; // Import Firebase configuration
import { doc, getDoc } from 'firebase/firestore'; // Import Firestore functions
import './HomePage.css';

function HomePage() {
  const [userData, setUserData] = useState(null);

  useEffect(() => {
    const fetchUserData = async () => {
      const user = auth.currentUser;
      if (user) {
        const userDoc = await getDoc(doc(db, "users", user.uid));
        if (userDoc.exists()) {
          setUserData(userDoc.data());
        }
      }
    };

    fetchUserData();
  }, []);

  if (!userData) {
    return <div>Loading...</div>;
  }

  return (
    <div className="home-container">
      <Sidebar />
      <div className="main-content">
        <header>
          <input type="text" placeholder="Search" className="search-bar" />
        </header>
        <h1>Welcome, {userData.nama}</h1>
        <section>
          <h2>Last Course:</h2>
          <div className="course-section">
            <CourseCard course={userData.lastcourse[0]} />
          </div>
        </section>
        <section>
          <h2>Progress:</h2>
          <div className="progress-cards">
            {userData.progresscourse.map((progress, index) => (
              <ProgressCard key={index} country={progress.country} progress={progress.progress} />
            ))}
          </div>
        </section>
        <section>
          <h2>Next recommended:</h2>
          <div className="next-recommended">
            {/* Example for static data */}
            <ProgressCard country="korea" progress="0%" />
            <ProgressCard country="china" progress="0%" />
          </div>
        </section>
      </div>
      <Reminder />
    </div>
  );
}

export default HomePage;
