// src/components/CoursePage.js
import React, { useState, useEffect } from 'react';
import './CoursePage.css';
import Sidebar from './Sidebar';
import Reminder from './Reminder';
import { auth, db } from './firebase';
import { doc, getDoc } from 'firebase/firestore';

function CoursePage() {
  const [userData, setUserData] = useState(null);

  useEffect(() => {
    const fetchUserData = async () => {
      const user = auth.currentUser;
      if (user) {
        const userDoc = await getDoc(doc(db, "users", user.uid));
        if (userDoc.exists()) {
          setUserData(userDoc.data());
        }
      }
    };

    fetchUserData();
  }, []);

  if (!userData) {
    return <div>Loading...</div>;
  }
  return (
    <div className="course-page">
        <Sidebar />
      <div className="main-content">
        <header>
            <input type="text" placeholder="Search" className="search-bar" />
        </header>
        <h1>Welcome, {userData.nama}</h1>
        <div className="section">
          <h2>Last Course:</h2>
          <div className="course-card">
            <img src="/images/course-image.jpg" alt="Course" />
            <div className="course-info">
              <h3>English Course 1</h3>
              <p>Lesson: 6</p>
              <p>Students: 98</p>
              <p>Level: Advanced</p>
              <p>Points: 25 / 100</p>
              <p>Days: 56</p>
              <button>Let's go</button>
            </div>
          </div>
          <div className="course-card">
            <img src="/images/course-image.jpg" alt="Course" />
            <div className="course-info">
              <h3>English Course 2</h3>
              <p>Lesson: 6</p>
              <p>Students: 98</p>
              <p>Level: Advanced</p>
              <p>Points: 25 / 100</p>
              <p>Days: 56</p>
              <button>Let's go</button>
            </div>
          </div>
          <div className="course-card">
            <img src="/images/course-image.jpg" alt="Course" />
            <div className="course-info">
              <h3>English Course 3</h3>
              <p>Lesson: 6</p>
              <p>Students: 98</p>
              <p>Level: Advanced</p>
              <p>Points: 25 / 100</p>
              <p>Days: 56</p>
              <button>Let's go</button>
            </div>
          </div>
          <div className="course-card">
            <img src="/images/course-image.jpg" alt="Course" />
            <div className="course-info">
              <h3>English Course 4</h3>
              <p>Lesson: 6</p>
              <p>Students: 98</p>
              <p>Level: Advanced</p>
              <p>Points: 25 / 100</p>
              <p>Days: 56</p>
              <button>Let's go</button>
            </div>
          </div>
        </div>
      </div>
      <Reminder />
    </div>
  );
}

export default CoursePage;
