import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { createUserWithEmailAndPassword } from 'firebase/auth';
import { auth, db } from './firebase'; // Tambahkan 'db' dari firebase konfigurasi
import { useNavigate } from 'react-router-dom';
import { doc, setDoc } from 'firebase/firestore'; // Impor Firestore functions
import './SignUpForm.css';

function SignUpForm() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [username, setUsername] = useState(''); // Tambahkan state untuk username
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const userCredential = await createUserWithEmailAndPassword(auth, email, password);
      const user = userCredential.user;

      // Simpan data pengguna ke Firestore
      await setDoc(doc(db, "users", user.uid), {
        email: email,
        nama: username,
        coursetaken: [],
        lastcourse: [],
        levelcourse: [],
        progresscourse: []
      });

      navigate('/SignIn');
    } catch (error) {
      console.error("Error signing up: ", error);
    }
  };

  return (
    <div className="SignUpForm">
      <form onSubmit={handleSubmit}>
      <h2>Sign Up</h2>
        <div className="input-group">
          <label>Username</label>
          <input 
            type="text" 
            value={username} 
            onChange={(e) => setUsername(e.target.value)} 
            required 
          />
        </div>
        <div className="input-group">
          <label>Email</label>
          <input 
            type="email" 
            value={email} 
            onChange={(e) => setEmail(e.target.value)} 
            required 
          />
        </div>
        <div className="input-group">
          <label>Password</label>
          <input 
            type="password" 
            value={password} 
            onChange={(e) => setPassword(e.target.value)} 
            required 
          />
        </div>
        <div className="input-group">
          <label>Confirm Password</label>
          <input type="password" name="confirmPassword" required />
        </div>
        <button type="submit">Sign Up</button>
      </form>
      <p>
        Already have an account? <Link to="/SignIn">Sign In!</Link>
      </p>
    </div>
  );
}

export default SignUpForm;
