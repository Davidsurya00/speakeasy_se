import React from 'react';
import './LanguagePage.css';

const languages = [
  { name: 'Indonesia', flag: 'https://storage.googleapis.com/speakeasy-3-a7aa7.appspot.com/flag/indonesia.jpg' },
  { name: 'English', flag: 'https://storage.googleapis.com/speakeasy-3-a7aa7.appspot.com/flag/english.jpg' },
  { name: 'Chinese', flag: 'https://storage.googleapis.com/speakeasy-3-a7aa7.appspot.com/flag/china.jpg' },
  { name: 'Thailand', flag: 'https://storage.googleapis.com/speakeasy-3-a7aa7.appspot.com/flag/thailand.jpg' },
  { name: 'Japanese', flag: 'https://storage.googleapis.com/speakeasy-3-a7aa7.appspot.com/flag/japan.jpg' },
  { name: 'German', flag: 'https://storage.googleapis.com/speakeasy-3-a7aa7.appspot.com/flag/german.jpg' },
  { name: 'Italian', flag: 'https://storage.googleapis.com/speakeasy-3-a7aa7.appspot.com/flag/italy.jpg' },
  { name: 'Swedish', flag: 'https://storage.googleapis.com/speakeasy-3-a7aa7.appspot.com/flag/swedia.jpg' },
  { name: 'Spanish', flag: 'https://storage.googleapis.com/speakeasy-3-a7aa7.appspot.com/flag/spain.jpg' },
  { name: 'French', flag: 'https://storage.googleapis.com/speakeasy-3-a7aa7.appspot.com/flag/french.jpg' },
  { name: 'Korean', flag: 'https://storage.googleapis.com/speakeasy-3-a7aa7.appspot.com/flag/korea.jpg' },
  { name: 'Russian', flag: 'https://storage.googleapis.com/speakeasy-3-a7aa7.appspot.com/flag/russia.jpg' },
];

function LanguagePage() {
  return (
    <div className="language-page">
      <header>
        <h1 className="logo">SpeakEasy</h1>
        <div className="language-selector">
          <span>Language: English</span>
          <select>
            <option>English</option>
            {/* Add more options here */}
          </select>
        </div>
      </header>
      <h2>I want to learn ..... language</h2>
      <div className="language-grid">
        {languages.map((language, index) => (
          <div key={index} className="language-card">
            <img src={language.flag} alt={`${language.name} flag`} />
            <p>{language.name}</p>
          </div>
        ))}
      </div>
    </div>
  );
}

export default LanguagePage;
