import React, { useState, useEffect } from 'react';
import { getDownloadURL, ref } from 'firebase/storage';
import { storage } from './firebase'; // Make sure to import from firebase.js
import './ProgressCard.css';

function ProgressCard({ country = '', progress = '0%' }) {
  const [imageUrl, setImageUrl] = useState('');

  useEffect(() => {
    const fetchImage = async () => {
      if (country) {
        const imageRef = ref(storage, `flag/${country.toLowerCase()}.jpg`);
        try {
          const url = await getDownloadURL(imageRef);
          console.log("Image URL fetched successfully: ", url); // Logging success
          setImageUrl(url);
        } catch (error) {
          console.error("Error fetching image URL: ", error); // Logging error
        }
      }
    };

    fetchImage();
  }, [country]);

  return (
    <div className="progress-card">
      {imageUrl ? (
        <img src={imageUrl} alt={country} />
      ) : (
        <p>Loading image...</p>
      )}
      <p>{progress}</p>
    </div>
  );
}

export default ProgressCard;
