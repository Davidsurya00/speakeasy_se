// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';
import { getStorage } from 'firebase/storage';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCwYiBfI4BSNnPfCDRHrJCPF_ZLTm68JQU",
  authDomain: "speakeasy-3-a7aa7.firebaseapp.com",
  projectId: "speakeasy-3-a7aa7",
  storageBucket: "speakeasy-3-a7aa7.appspot.com",
  messagingSenderId: "742628801304",
  appId: "1:742628801304:web:beaad405c9c1cc7c363c6d"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getFirestore(app);
const storage = getStorage(app); // Initialize Firebase Storage

export { auth, db, storage };