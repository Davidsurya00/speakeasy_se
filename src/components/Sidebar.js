import React from 'react';
import { Link } from 'react-router-dom';
import './Sidebar.css';

function Sidebar() {
  return (
    <div className="sidebar">
      <h1 className="logo">SpeakEasy</h1>
      <ul>
        <li><Link to="/Home">Home</Link></li>
        <li><Link to="/Course">Course</Link></li>
        <li><Link to="/Shop">Shop</Link></li>
        <li><Link to="/Setting">Setting</Link></li>
      </ul>
    </div>
  );
}

export default Sidebar;
