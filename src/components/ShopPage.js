import React, { useState, useEffect } from 'react';
import { getDownloadURL, ref } from 'firebase/storage';
import { storage } from './firebase';
import Sidebar from './Sidebar';
import Reminder from './Reminder';
import './ShopPage.css';

function ShopPage() {
  const languages = [
    { name: 'English', flagPath: 'flag/english.jpg' },
    { name: 'Chinese', flagPath: 'flag/china.jpg' },
    { name: 'Korean', flagPath: 'flag/korea.jpg' },
    { name: 'Japanese', flagPath: 'flag/japan.jpg' },
    { name: 'Italian', flagPath: 'flag/italy.jpg' },
    { name: 'German', flagPath: 'flag/german.jpg' },
    { name: 'French', flagPath: 'flag/french.jpg' },
    { name: 'Russian', flagPath: 'flag/russian.jpg' }
  ];

  const [flags, setFlags] = useState({});

  useEffect(() => {
    const fetchFlags = async () => {
      const newFlags = {};
      for (const language of languages) {
        try {
          const url = await getDownloadURL(ref(storage, language.flagPath));
          newFlags[language.name] = url;
        } catch (error) {
          console.error(`Error loading ${language.name} image`, error);
        }
      }
      setFlags(newFlags);
    };

    fetchFlags();
  }, [languages]); // Added languages to dependency array

  return (
    <div className="shop-container">
      <Sidebar />
      <div className="main-content">
        <header>
          <input type="text" placeholder="Search" className="search-bar" />
        </header>
        <h1 className="shop-title">Shop</h1>
        <h2 className="choose-language">Choose Language:</h2>
        <div className="languages-grid">
          {languages.map((language, index) => (
            <div className="language-card" key={index}>
              <img src={flags[language.name]} alt={language.name} onError={(e) => console.error(`Error loading ${language.name} image`)} />
              <span>{language.name}</span>
            </div>
          ))}
        </div>
      </div>
      <Reminder />
    </div>
  );
}

export default ShopPage;
