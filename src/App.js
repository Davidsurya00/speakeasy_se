import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import SignInForm from './components/SignInForm';
import SignUpForm from './components/SignUpForm';
import HomePage from './components/HomePage';
import CoursePage from './components/CoursePage';
import ShopPage from './components/ShopPage';
import SettingsPage from './components/SettingsPage';
import LanguagePage from './components/LanguagePage';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Router>
          <Routes>
            <Route path="/" element={<Navigate to="/SignIn" />} />
            <Route path="/SignUp" element={<SignUpForm />} />
            <Route path="/SignIn" element={<SignInForm />} />
            <Route path="/Home" element={<HomePage />} />
            <Route path="/Course" element={<CoursePage />} />
            <Route path="/Shop" element={<ShopPage />} />
            <Route path="/Setting" element={<SettingsPage />} />
            <Route path="/Language" element={<LanguagePage />} />
          </Routes>
        </Router>
      </header>
    </div>
  );
}

export default App;